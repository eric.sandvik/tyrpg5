export const TY_RPG5 = {};

/**
 * The set of Ability Scores used within the system.
 * @type {Object}
 */
TY_RPG5.abilities = {
  str: 'TY_RPG5.Ability.Str.long',
  dex: 'TY_RPG5.Ability.Dex.long',
  con: 'TY_RPG5.Ability.Con.long',
  int: 'TY_RPG5.Ability.Int.long',
  wis: 'TY_RPG5.Ability.Wis.long',
  cha: 'TY_RPG5.Ability.Cha.long',
};

TY_RPG5.abilityAbbreviations = {
  str: 'TY_RPG5.Ability.Str.abbr',
  dex: 'TY_RPG5.Ability.Dex.abbr',
  con: 'TY_RPG5.Ability.Con.abbr',
  int: 'TY_RPG5.Ability.Int.abbr',
  wis: 'TY_RPG5.Ability.Wis.abbr',
  cha: 'TY_RPG5.Ability.Cha.abbr',
};
